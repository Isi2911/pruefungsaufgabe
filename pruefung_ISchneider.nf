nextflow.enable.dsl = 2


params.database = "./CARD_v3.0.8_SRST2.fasta"

process fastp{
    publishDir "${params.outdir}/fastp_results", mode: "copy" , overwrite: true 
    container "https://depot.galaxyproject.org/singularity/fastp%3A0.22.0--h2e03b76_0" 
    input:
        path fastqfiles
    output:
        path "${fastqfiles.getSimpleName()}_fastp_trimmed.fastq", emit: fastp_trimmed_fastqfiles
        path "${fastqfiles.getSimpleName()}_fastp.json", emit: fastp_json_files
    script:
    """
    fastp -i ${fastqfiles} -o ${fastqfiles.getSimpleName()}_fastp_trimmed.fastq -j ${fastqfiles.getSimpleName()}_fastp.json
    """
}


process fastqc{
    publishDir "${params.outdir}", mode: "copy" , overwrite: true 
    container "https://depot.galaxyproject.org/singularity/fastqc%3A0.11.9--hdfd78af_1"
    input:
        path fastp_channel
    output:
        path "fastQC_results/*"
    script:
    """
    mkdir fastQC_results
    fastqc  ${fastp_channel} --outdir fastQC_results
    """
}

process multiQC{
    publishDir "${params.outdir}/multiQC", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/multiqc%3A1.9--pyh9f0ad1d_0"
    input:
        path infile
    output:
        path "*"
    script:
    """
    multiqc ${infile}
    """
}

process srst2{
    publishDir "${params.outdir}", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/srst2%3A0.2.0--py27_2"
    input:
        path srst2_channel

    output:
        path "srst2_results/*__genes*results.txt",  emit:gene_results
    script:
    """
    srst2 --input_se ${srst2_channel[0]} --output ./srst2_results/${srst2_channel[0].getSimpleName()}  --log --gene_db ${srst2_channel[1]}
    """

}


process srst2_gesamt{
    publishDir "${params.outdir}/srst2_results", mode: "copy" , overwrite: true // erschafft nur Ordner

    input:
        path srst2_channel

    output:
        path "Ergebnisdatei.txt"
    script:
    """
    cat ${srst2_channel} >> Ergebnisdatei.txt
    """

}




workflow {
  fastqfiles = channel.fromPath("./rawdata/*")
  //fastqfiles.view()
  
  fastp_channel = fastp(fastqfiles)
  
  fastp_channel_fastq = fastp_channel.fastp_trimmed_fastqfiles
  fastp_channel_json = fastp_channel.fastp_json_files
  //fastp_channel_fastq.view()
  //fastp_channel_json.view()
  fastqc_channel = fastqc(fastp_channel_fastq)
  //multiQC_channel = multiQC(fastp_channel_json.collect())
  multiQC_channel = multiQC(fastqc_channel.collect().concat(fastp_channel_json).collect())

  database_channel = channel.fromPath(params.database)

  srst2_channel = fastp_channel_fastq.combine(database_channel)
  //srst2_channel.view()

  srst2_channel_2 = srst2(srst2_channel)
  srst2_channel_3 = srst2_channel_2.gene_results
  //srst2_channel_3.view()
  srst2_gesamt(srst2_channel_3.collect())

}


// nextflow run pruefung_ISchneider.nf -profile singularity --outdir ./out -with-dag ./dag.html -with-timeline ./timeline.html








